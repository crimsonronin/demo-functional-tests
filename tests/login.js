var loginModule = require('../lib/modules/header/login');

module.exports = {
    tags: ['kpmg'],
    'Ensures failed login shows error message': function(client) {
        client.
            url('https://cherrywood.kpmg.etaskr.com/').
            waitForElementVisible(loginModule.base, 1000).
            click(loginModule.loginButton).
            waitForElementVisible(loginModule.modal.base, 1000).
            setValue(loginModule.modal.form.email, 'test-kpmg@example.com').
            setValue(loginModule.modal.form.password, 'password').
            click(loginModule.modal.form.submitButton).
            waitForElementVisible(loginModule.modal.alert, 1000).
            assert.containsText(loginModule.modal.alert, 'Please check your credentials');

        client.end();
    }
};
