# Functional Tests Demo

This is just a demo / skeleton app to show how to create functional tests using [nightwatch](http://nightwatchjs.org/).

## Install

```
git clone https://crimsonronin@bitbucket.org/crimsonronin/demo-functional-tests.git
cd demo-functional-tests
npm install
```

To run the demo test just:

```
npm test
```

The demo test will:

* Go to https://marketplace.kpmg.com.au/
* Click the login button
* Input a bad email / password
* Click submit
* Assert that the error message appears


### Modules

I started creating a module structure `./lib/modules` to help with reusability between tests. This borrows a little from [Geb](http://www.gebish.org/testing), a Java functional testing framework. 

So when you create a test, instead of hardcoding the element selectors within the test, you should create or extend an existing "module" which represents a specific section of the page.

eg. Instead of having the following in the test:

```
client.
    url('https://marketplace.kpmg.com.au').
    click('#login-nav a[href="/login"]');
```

You create a module called `./lib/modules/header/login.js` which defines the login button selector eg.

```
module.exports = {
    loginButton: '#login-nav a[href="/login"]'
};
```

It can now be reused in other tests easily.

```
var loginModule = require('../lib/modules/header/login');

client.
    url('https://marketplace.kpmg.com.au').
    click(loginModule.loginButton);
```

#### Helpers

You could also create helper functions that do common tasks such as:

* logging in
* filling in forms
* navigation
* etc

```
var loginModule = require('./lib/modules/header/login');
module.exports = {
    login: function(client) {
        client.
            url('https://marketplace.kpmg.com.au/login').
            waitForElementVisible(loginModule.modal.base, 1000).
            setValue(loginModule.modal.form.email, 'test-kpmg@example.com').
            setValue(loginModule.modal.form.password, 'password').
            click(loginModule.modal.form.submitButton);
            
            //return some promise etc so we know when to process the next task
    }
};
```