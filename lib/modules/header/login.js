module.exports = {
    base: '#login-nav',
    loginButton: '#login-nav a[href="/login"]',
    modal: {
        base: '#modal',
        alert: '#modal .login-alert',
        form: {
            email: '#modal input[name="email"]',
            password: '#modal input[name="pass"]',
            submitButton: '#modal button[type="submit"]'
        }
    }
};
